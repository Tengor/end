import java.awt.Font;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class JPanelApp extends JPanel
{
    JTextField txt1 = null;
    int res = 0;
    int res1 = 0;
    String op = "";

    public JPanelApp()
    {
        try
        {
            setLayout(null);
            // Specifies the position of the element
            final TextField txt1 = new TextField();
            txt1.setBounds(10, 10, 365, 50);

            JButton bCleanA = new JButton("AC");
            bCleanA.setBounds(60, 70, 75, 50);

            JButton bCleanO = new JButton("<");
            bCleanO.setBounds(170, 70, 75, 50);

            JButton b0 = new JButton("0");
            b0.setBounds(125, 330, 50, 50);

            JButton b1 = new JButton("1");
            b1.setBounds(50, 265, 50, 50);

            JButton b2 = new JButton("2");
            b2.setBounds(125, 265, 50, 50);

            JButton b3 = new JButton("3");
            b3.setBounds(200, 265, 50, 50);

            JButton b4 = new JButton("4");
            b4.setBounds(50, 200, 50, 50);

            JButton b5 = new JButton("5");
            b5.setBounds(125, 200, 50, 50);

            JButton b6 = new JButton("6");
            b6.setBounds(200, 200, 50, 50);

            JButton b7 = new JButton("7");
            b7.setBounds(50, 135, 50, 50);

            JButton b8 = new JButton("8");
            b8.setBounds(125, 135, 50, 50);

            JButton b9 = new JButton("9");
            b9.setBounds(200, 135, 50, 50);

            JButton bDot = new JButton(".");
            bDot.setBounds(200, 330, 50, 50);

            JButton bDivision = new JButton("/");
            bDivision.setBounds(280, 70, 75, 50);
            Font bigFontDivision = new Font("serif", Font.BOLD, 22);
            bDivision.setFont(bigFontDivision);

            JButton bMulti = new JButton("*");
            bMulti.setBounds(280, 135, 75, 50);
            Font bigFontMulti = new Font("serif", Font.BOLD, 22);
            bMulti.setFont(bigFontMulti);

            JButton bMinus = new JButton("-");
            bMinus.setBounds(280, 200, 75, 50);
            Font bigFontMinus = new Font("serif", Font.BOLD, 22);
            bMinus.setFont(bigFontMinus);

            JButton bPlus = new JButton("+");
            bPlus.setBounds(280, 265, 75, 50);
            Font bigFontPlus = new Font("serif", Font.BOLD, 22);
            bPlus.setFont(bigFontPlus);

            JButton bRes = new JButton("=");
            bRes.setBounds(280, 330, 75, 50);
            Font bigFont = new Font("serif", Font.BOLD, 22);
            bRes.setFont(bigFont);





            add(txt1);
            add(b0);
            add(b1);
            add(b2);
            add(b3);
            add(b4);
            add(b5);
            add(b6);
            add(b7);
            add(b8);
            add(b9);
            add(bCleanA);
            add(bCleanO);
            add(bDot);
            add(bRes);
            add(bPlus);
            add(bMinus);
            add(bMulti);
            add(bDivision);

            bCleanA.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    txt1.setText(null);
                    txt1.setText(null);
                }
            });

            b1.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    txt1.setText(txt1.getText() + 1);
                    if (res==0) {
                        res = Integer.valueOf(txt1.getText());
                    } else {
                        res1 = Integer.valueOf(txt1.getText());
                    }
                }
            });

            b2.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    txt1.setText(txt1.getText() + 2);
                    if (res==0) {
                        res = Integer.valueOf(txt1.getText());
                    } else {
                        res1 = Integer.valueOf(txt1.getText());
                    }}
            });

            b3.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    txt1.setText(txt1.getText() + 3);
                    if (res==0) {
                        res = Integer.valueOf(txt1.getText());
                    } else {
                        res1 = Integer.valueOf(txt1.getText());
                    }
                }
            });

            b4.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    txt1.setText(txt1.getText() + 4);
                    if (res==0)
                    {
                        res = Integer.valueOf(txt1.getText());
                    } else {
                        res1 = Integer.valueOf(txt1.getText());
                    }
                }
            });

            b5.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    txt1.setText(txt1.getText() + 5);
                    if (res==0) {
                        res = Integer.valueOf(txt1.getText());
                    } else {
                        res1 = Integer.valueOf(txt1.getText());
                    }
                }
            });

            b6.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    txt1.setText(txt1.getText() + 6);
                    if (res==0) {
                        res = Integer.valueOf(txt1.getText());
                    } else {
                        res1 = Integer.valueOf(txt1.getText());
                    }
                }
            });

            b7.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    txt1.setText(txt1.getText() + 7);
                    if (res==0) {
                        res = Integer.valueOf(txt1.getText());
                    } else {
                        res1 = Integer.valueOf(txt1.getText());
                    }
                }
            });

            b8.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    txt1.setText(txt1.getText() + 8);
                    if (res==0) {
                        res = Integer.valueOf(txt1.getText());
                    } else {
                        res1 = Integer.valueOf(txt1.getText());
                    }
                }
            });

            b9.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    txt1.setText(txt1.getText() + 9);
                    if (res==0) {
                        res = Integer.valueOf(txt1.getText());
                    } else {
                        res1 = Integer.valueOf(txt1.getText());
                    }
                }
            });

            b0.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    txt1.setText(txt1.getText() + 0);
                    if (res==0) {
                        res = Integer.valueOf(txt1.getText());
                    } else {
                        res1 = Integer.valueOf(txt1.getText());
                    }
                }
            });

            bPlus.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Integer.valueOf(txt1.getText());txt1.setText("");
                    op = "+";
                }
            });

            bMinus.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Integer.valueOf(txt1.getText());
                    txt1.setText("");
                    op = "-";
                }
            });

            bMulti.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Integer.valueOf(txt1.getText());
                    txt1.setText("");
                    op = "*";
                }
            });

            bDivision.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg1)
                {
                    res = Integer.valueOf(txt1.getText());
                    txt1.setText("");
                    op = "/";
                }
            });

            bRes.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent arg0)
                {
                    int num = res;
                    int num1 = res1;
                    String strOp = op;

                    MetodCalc mc = new MetodCalc();
                    String strRes = String.valueOf(mc.calc(num, strOp, num1 ));
                    txt1.setText(strRes);
                }
            });
        }
        catch (ArithmeticException exception)
        {
            //System.out.println("Can not divide by zero ");
        }
    }
}