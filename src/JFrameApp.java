import javax.swing.JFrame;

public class JFrameApp extends JFrame
{
    public JFrameApp()
    {
        setBounds(100, 100, 400, 500);
        setTitle("Calculator");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add( new JPanelApp());
        setVisible(true);
    }
}